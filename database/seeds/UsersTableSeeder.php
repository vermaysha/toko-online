<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for ($i = 1; $i <= 10; $i++) {
            DB::table('users')->insert([
                'username'   => ($i == 1) ? 'admin' : $faker->username,
                'email'      => ($i == 1) ? 'admin@gmail.com' : $faker->email,
                'password'   => Hash::make('12345678'),
                'level'      => ($i == 1) ? 'admin' : 'buyer',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => null
            ]);

            $users_meta = [
                'fullname'     => $faker->name('male'),
                'gender'       => $faker->randomElement(['male', 'female']),
                'address'      => $faker->address,
                'phone_number' => $faker->phoneNumber,
                'company'      => $faker->company,
                'birth'        => '21-01-1995',
                'about'        => $faker->realText($faker->numberBetween(50,60))
            ];

            foreach ($users_meta as $meta_key => $meta_val) {
                DB::table('users_meta')->insert([
                    'meta_key' => $meta_key,
                    'meta_value' => $meta_val,
                    'user_id' => $i,
                    'created_at' => now(),
                    'updated_at' => now(),
                    'deleted_at' => null
                ]);
            }
        }
    }
}

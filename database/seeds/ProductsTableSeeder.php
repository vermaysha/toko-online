<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for ($i = 1; $i < 22; $i++) {
            DB::table('products')->insert([
                'name'        => $faker->words(3, true),
                'file_name'   => 'product-1.png',
                'description' => $faker->realText(200),
                'price'       => $faker->randomNumber(7),
                'stock'       => $faker->randomNumber(2),
                'category_id' => $faker->numberBetween(1, 5),
                'created_at'  => now(),
                'updated_at'  => now(),
                'deleted_at'  => null
            ]);
        }
    }
}

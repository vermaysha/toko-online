<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'name' => 'Veggies',
                'slug' =>  'veggies',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Fruits',
                'slug' =>  'fruits',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Foods',
                'slug' =>  'foods',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Bread & Bakery',
                'slug' =>  'bread-bakery',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Pet Foods',
                'slug' =>  'foods',
                'created_at' => now(),
                'updated_at' => now()
            ]
        ];

        foreach ($categories as $cat) {
            DB::table('categories')->insert($cat);
        }

    }
}

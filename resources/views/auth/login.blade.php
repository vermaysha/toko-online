@extends('layouts.app')

@section('products-breadcrumb')
<!-- products-breadcrumb -->
<div class="products-breadcrumb">
  <div class="container">
    <ul>
      <li><i class="fa fa-home" aria-hidden="true"></i><a href="index.html">Home</a><span>|</span></li>
      <li>Sign In & Sign Up</li>
    </ul>
  </div>
</div>
<!-- //products-breadcrumb -->
@endsection

@section('banner-content')
<div class="login">
    <h3>Sign In & Sign Up</h3>
    <div class="login_module">
        <div class="module form-module">
            <div class="toggle"><i class="fa fa-times fa-pencil"></i>
                <div class="tooltip">Sign up</div>
            </div>
            <div class="form">
                <h2>Login to your account</h2>
                <form action="{{ url('login') }}" method="post">
                    @csrf
                    @if ($errors->any())
                        @foreach ($errors->all() as $error)
                        <div class="alert alert-danger">
                            {{ $error }}
                        </div>
                        @endforeach
                    @endif
                    <input type="text" name="username" placeholder="Username" required=" ">
                    <input type="password" name="password" placeholder="Password" required=" ">
                    <input type="submit" value="Login">
                </form>
            </div>
            <div class="form">
                <h2>Create an account</h2>
                <form action="{{ url('register') }}" method="post">
                    <input type="text" name="Username" placeholder="Username" required=" ">
                    <input type="password" name="Password" placeholder="Password" required=" ">
                    <input type="email" name="Email" placeholder="Email Address" required=" ">
                    <input type="text" name="Phone" placeholder="Phone Number" required=" ">
                    <input type="submit" value="Register">
                </form>
            </div>
            <div class="cta"><a href="#">Forgot your password?</a></div>
        </div>
    </div>
    <script>
    $('.toggle').click(function(){
      // Switches the Icon
      $(this).children('i').toggleClass('fa-pencil');
      // Switches the forms
      $('.form').animate({
        height: "toggle",
        'padding-top': 'toggle',
        'padding-bottom': 'toggle',
        opacity: "toggle"
      }, "slow");
    });
    </script>
</div>
@endsection

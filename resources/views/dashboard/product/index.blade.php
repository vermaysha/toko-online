@extends('layouts.admin-panel')

@section('breadcrumbs')
<h1>Product <small>Manage data</small></h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Product</li>
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Manage Product</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="manage-data" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Product name</th>
                            <th>Harga</th>
                            <th>Stock</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($products as $prod)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $prod->name }}</td>
                            <td>{{ $prod->price }}</td>
                            <td>{{ $prod->stock }}</td>
                            <td><a href="{{ route('dashboard.show-product', $prod->id) }}" class="btn btn-sm btn-info">Edit</a> <a href="{{ route('dashboard.delete-product', $prod->id) }}" class="btn btn-sm btn-danger">Hapus</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Product name</th>
                            <th>Harga</th>
                            <th>Stock</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>
@endsection

@section('javascript')
@endsection

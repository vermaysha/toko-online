@extends('layouts.admin-panel')

@section('breadcrumbs')
<h1>Product <small>Add new product</small></h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#"><i class="fa fa-dashboard"></i> Product</a></li>
    <li class="active">Add product</li>
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Add new product</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" action="{{ url()->current() }}" enctype="multipart/form-data" autocomplete="off">
              @csrf
              <div class="box-body">
                @if ($errors->any())
                    @foreach ($errors->all() as $error)
                    <div class="alert alert-danger">
                        {{ $error }}
                    </div>
                    @endforeach
                @endif
                <div class="form-group">
                  <label class="col-sm-2 control-label">Nama produk</label>

                  <div class="col-sm-10">
                    <input type="text" name="name" class="form-control" placeholder="Nama produk" required="" />
                  </div>
                </div>
                 <div class="form-group">
                  <label class="col-sm-2 control-label">Harga produk</label>

                  <div class="col-sm-10">
                    <input type="text" name="price" class="form-control" placeholder="harga produk" required="" data-type="currency" />
                  </div>
                </div>
                 <div class="form-group">
                  <label class="col-sm-2 control-label">Stok produk</label>

                  <div class="col-sm-10">
                    <input type="text" name="stock"  class="form-control" placeholder="Stok produk" required="" data-type="currency" />
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Foto produk</label>

                  <div class="col-sm-10">
                    <input type="file" name="picture" class="form-control" placeholder="select file" accept="image/*" required="" />
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Kategori</label>

                  <div class="col-sm-10">
                    <select class="form-control" name="category" required="">
                        <option value="">-- Select Category --</option>
                      @foreach ($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="{{ (url()->current() == url()->previous()) ? route('dashboard.product') : url()->previous() }}" class="btn btn-default">Cancel</a>
                <button type="submit" class="btn btn-info pull-right">Add product</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
</div>
@endsection

@section('javascript')
<script>
  $("input[data-type='currency']").on({
    keyup: function() {
      formatCurrency($(this));
    },
    blur: function() {
      formatCurrency($(this), "blur");
    }
});


function formatNumber(n) {
  // format number 1000000 to 1,234,567
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")
}


function formatCurrency(input, blur) {
  // and puts cursor back in right position.

  // get input value
  var input_val = input.val();

  // don't validate empty input
  if (input_val === "") { return; }

  // original length
  var original_len = input_val.length;

  // initial caret position
  var caret_pos = input.prop("selectionStart");

  // check for decimal
  // if (input_val.indexOf(",") >= 0) {

  //   // get position of first decimal
  //   // this prevents multiple decimals from
  //   // being entered
  //   var decimal_pos = input_val.indexOf(",");

  //   // split number by decimal point
  //   var left_side = input_val.substring(0, decimal_pos);
  //   var right_side = input_val.substring(decimal_pos);

  //   // add commas to left side of number
  //   left_side = formatNumber(left_side);

  //   // validate right side
  //   right_side = formatNumber(right_side);

  //   // On blur make sure 2 numbers after decimal
  //   if (blur === "blur") {
  //     right_side += "00";
  //   }

  //   // Limit decimal to only 2 digits
  //   right_side = right_side.substring(0, 2);

  //   // join number by .
  //   input_val = left_side + "," + right_side;

  // } else {
    // no decimal entered
    // add commas to number
    // remove all non-digits
    input_val = formatNumber(input_val);
    input_val = input_val;

  //   // final formatting
  //   if (blur === "blur") {
  //     input_val += "00";
  //   }
  // }

  // send updated string to input
  input.val(input_val);

  // put caret back in the right position
  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}
</script>
@endsection

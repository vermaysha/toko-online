<?php
namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use App\User;

class UserHelper {

    public static function get_username($user_id) {
        $user = DB::table('users')->where('userid', $user_id)->first();
        return (isset($user->username) ? $user->username : '');
    }

    public static function meta(\Illuminate\Database\Eloquent\Collection $user) {
        $metas = [];
        foreach ($user as $key) {
            $metas[$key->meta_key] = $key->meta_value;
        }

        return collect($metas);
    }
}

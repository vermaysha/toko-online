<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Category;

class ProductController extends Controller
{

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.product.index', [
            'products' => Product::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.product.create', [
            'categories' => Category::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'     => 'bail|required|regex:/[a-zA-Z0-9 ().]+/',
            'price'    => 'bail|required',
            'stock'    => 'bail|required',
            'picture'  => 'bail|image',
            'category' => 'bail|required'
        ]);

        $name     = $request->input('name');
        $price    = str_replace('.', null, $request->input('price'));
        $stock    = str_replace('.', null, $request->input('stock'));
        $picture  = $request->file('picture');
        $category = $request->input('category');

        // $file = $request->file('picture');

        // echo 'File Name: '.$file->getClientOriginalName();
        // echo '<br>';

        //         // ekstensi file
        // echo 'File Extension: '.$file->getClientOriginalExtension();
        // echo '<br>';

        //         // real path
        // echo 'File Real Path: '.$file->getRealPath();
        // echo '<br>';

        // //         // ukuran file
        // echo 'File Size: '.$file->getSize();
        // echo '<br>';

        //         // tipe mime
        // echo 'File Mime Type: '.$file->getMimeType();

        //         // isi dengan nama folder tempat kemana file diupload
        $tujuan_upload = 'images/products';

                // upload file
        $picture->move($tujuan_upload,$picture->getClientOriginalName());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('dashboard.product.edit', [
            'categories' => Category::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();
    }
}

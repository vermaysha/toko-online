<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::name('dashboard.')->namespace('Dashboard')->prefix('dashboard')->group(function () {
    // Controllers Within The "App\Http\Controllers\Dashboard" Namespace
    Route::get('/product', 'ProductController@index')->name('product');
    Route::get('/product/add', 'ProductController@create')->name('add-product');
    Route::post('/product/add', 'ProductController@store');
    Route::get('/product/{id}', 'ProductController@edit')->name('show-product');
    Route::post('/product/{id}', 'ProductController@update');
    Route::get('/product/delete/{id}', 'ProductController@destroy')->name('delete-product');
});
